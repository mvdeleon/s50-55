// import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
	
	

	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(10)

	let { name, description, price, _id } = courseProp;

	// function enroll(){

	// 	if(count !== 10){
	// 		setCount(count + 1);
	// 		setSeats(seats - 1);
	// 		}

	// 	}
		
	// 	useEffect(()=>{
	// 		if(seats===0){
	// 			alert("No more seats available!")
	// 		}
	// 	},[count,seats])

	return(
		<Card className="p-3 mb-3">
			<Card.Body className="card-purple">
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				<Card.Text>Enrollees:  </Card.Text>
				<Button as={Link} to={`/courses/${_id}`}>Details</Button>
				
			</Card.Body>
		</Card>
	)
}